import React  from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

export default function App() {
  const [name, setName] = React.useState('');

  return (
    <View style={styles.container}>
      <Text>What is your name?</Text>
      <TextInput style={styles.input}
      onChangeText={(val) => setName(val)} />
      
      <Text>Hi {name} from Gyalpozhing College of Information Technology!</Text>
      <TextInput style={styles.input}
      placeholder='........................................' />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: '#777',
    padding: 8,
    margin: 10,
    width: 200,
  },
});
