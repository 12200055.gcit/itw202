import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        alignItems: 'center',
        justifyContent: 'center',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
      },
  })
  



export default styles

