import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyDJmBlcw211pFsE6XXv6QHKYRMmXTmpqrM",
  authDomain: "project-8826d.firebaseapp.com",
  databaseURL: "https://project-8826d-default-rtdb.firebaseio.com",
  projectId: "project-8826d",
  storageBucket: "project-8826d.appspot.com",
  messagingSenderId: "508043127148",
  appId: "1:508043127148:web:ccb5f42bdb025552ef6b49",
  measurementId: "G-TW1S6RFC8H"
};

firebase.initializeApp(CONFIG);

export default firebase;
