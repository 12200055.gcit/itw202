import React, {useState} from "react";
import { StyleSheet,View, TextInput, Button, Modal } from "react-native";

const AspirationInput = (props) => {
    const [enteredAspiration, setEnteredAspiration] = useState('');


    const AspirationInputHandler =(enteredText) => {
        setEnteredAspiration(enteredText);
      };
     
      const addAspirationHandler = () => {
        props.onAddAspiration(enteredAspiration);
        setEnteredAspiration('')
      }

  return (
    <Modal visible={props.visible}>
        <View style={styles.inputContainer}>
            <TextInput
            placeholder='My Aspiration from this module' 
            style={styles.input}
            onChangeText={AspirationInputHandler}
            value={enteredAspiration}
            />
            <View style={styles.buttonView}>
              <View style={styles.button}>
              <Button
                title='ADD'
                onPress={addAspirationHandler}
                />
              </View>
             <View>
               <Button
                title="CANCEL"
                color ="red"
                onPress={props.onCancel}
               />
             </View>
            </View>
        </View>
    </Modal>
  )
}
export default AspirationInput
const styles = StyleSheet.create({
    inputContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      input: {
        width: '80%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        marginBottom: 10
      },
      buttonView: {
        flexDirection: 'row',
        justifyContent:'space-between',
        width: '50%'
      },
      button:{
        width: '30%'

      }

})