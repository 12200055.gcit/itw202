import { StyleSheet, Text, View } from 'react-native';
import ViewComp from './component/ViewComp';

export default function App() {
  return (
    <View style={styles.container}>
      <ViewComp></ViewComp>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
