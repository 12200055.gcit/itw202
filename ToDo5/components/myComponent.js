import React from "react";
import { View, Text, StyleSheet } from 'react-native';

const MyComponent = () => {
    const name = "Khamsum Karma Wangmo";

    return (
        <View style = {StyleSheet.container}>
            <Text style = {StyleSheet.textStyle1}>
                Getting started with React Native!
            </Text>
            <Text style = {StyleSheet.textStyle2}>
                My name is {name}
            </Text>
        </View>
    )
};

const styles = StyleSheet.create({
    textStyle1: {
        fontSize: 45,
    },
    textStyle2: {
        fontSize: 20,
    }
});

export default MyComponent;