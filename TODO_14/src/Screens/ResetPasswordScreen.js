import React, {useState} from "react";
import { View, StyleSheet, Text } from 'react-native'
import Button from "../components/Button";
import Header from "../components/Header";
import Paragraph from "../components/Paragraph";
import Background from "../components/Background";
import Logo from "../components/Logo";
import TextInput from "../components/TextInput";
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/passwordValidator";
import BackButton from "../components/BackButton";
import { TouchableOpacity } from "react-native-gesture-handler";
import { theme } from "../core/theme";
import { sendEmailWithPassword } from "../api/auth-api";

export default function ResetPasswordScreen({navigation}){
    const [email, setEmail] = useState({value: "", error: ""})
    const [loading, setLoading] = useState();


    const onSubmitPressed = async() => {
        const emailError = emailValidator(email.value);
        if (emailError) {
            setEmail({...email, error: emailError });
        }
    setLoading(true)
    const response = await sendEmailWithPassword(email.value);
    if (response.error) {
        alert(response.error);
    } else {
        alert("Email with password has been sent. ");
    }
    setLoading(false)

    }
    
    return (
        <Background>
            <BackButton goBack={navigation.goBack} />
            <Logo />
            <Header>Restore Password</Header>
            <TextInput 
               label="Email" 
               value={email.value}
               error={email.error}
               errorText={email.error}
               onChangeText={(text) => setEmail({ value: text, error: "" })}
               description="You will receive email with password reset link."
            />
           
            <Button mode="contained" onPress = {onSubmitPressed}>Send Instructions</Button>
           
        </Background>
    )
}

