import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const MyComponent = () => {
  
    const greeting = 'Sonam Wangmo';
    const greeting1 = <Text>Khamsum Karma Wangmo </Text>
  return (
    <View>
      <Text style={styles.textStyle} >This is a demo of JSX</Text>
      <Text>Hi There!!!  {greeting}</Text>
      {greeting1}
    </View>
  )
}

export default MyComponent

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 24
    }
})