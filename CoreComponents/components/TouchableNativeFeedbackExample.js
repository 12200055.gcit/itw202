import React, {useState} from 'react'
import { StyleSheet, Text, View, TouchableNativeFeedback } from 'react-native'

const TouchableNativeFeedbackExample = () => {
    const [count, setCount] = useState(0);
  return (
    <View style={{padding: 100}}>
      <Text>You clicked {count} times</Text>
      <TouchableNativeFeedback
      style={styles.button}
      onPress={() => setCount(count + 1)}>
          <Text>Count</Text>
      </TouchableNativeFeedback>
    </View>
  )
}

export default TouchableNativeFeedbackExample

const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10
    }
})