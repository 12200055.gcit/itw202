import { View, Text } from 'react-native'
import React from 'react'

export default function TextComponent() {
  return (
    <View style={{ padding: 100 }}>
      <Text style={{ height: 50, borderWidth: 2 }}>
          Here's some text!
        </Text>
    </View>
  )
}