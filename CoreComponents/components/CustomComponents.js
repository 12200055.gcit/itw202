import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { COLORS } from '../constants/Colors'
import { RowItem } from './RowItem'

function CustomComponents() {
  return (
    <View style={styles.container}>
        <RowItem 
          text="Themes"
        />
        <RowItem 
          text="React Native Basics" 
        />
        <RowItem 
           text="React Native by Example"
        />  
    </View>
  )
}

export default CustomComponents

const styles = StyleSheet.create({
    container: {
        marginTop: 30
    },
    row: {
        paddingHorizontal: 20,
        paddingVertical: 16,
        justifyContent: 'space-between',
        alignItems: "center",
        flexDirection: 'row',
        backgroundColor: COLORS.white,
    },
    text: {
        color: COLORS.black,
        fontSize: 16,
    }
})