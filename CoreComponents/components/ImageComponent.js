import { StyleSheet, View, Image } from 'react-native'
import React from 'react'

const ImageComponent = () => {
  return (
    <View>
        <Image
        style={styles.logoContain}
        source={require('../assets/favicon.png')}
        />
        <Image
        style={styles.logoStretch}
        source={{uri: 'https://about.gitlab.com/images/topics/devops-lifecycle.png'}}
        />
    </View>
  )
}

export default ImageComponent

const styles = StyleSheet.create({
    logoContain: {
        width: 200,
        height: 100,
        resizeMode: 'contain',
    },
    logoStretch: {
        width: 200, 
        height: 100,
        resizeMode: 'stretch',
    },
})
