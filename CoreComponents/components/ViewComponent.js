import React from 'react'
import { Text, View } from 'react-native'
export default function ViewComponent() {
    return (
        <View style={{ flex: 1 }} >
            <View style={{ padding: 100, backgroundColor: 'pink' }}>
                <Text style={{ color: 'red' }}>Text with background color</Text>
            </View>
            <View style={{ margin: 16 }} />
        </View>
    )
}