import React, {useState} from 'react'
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native'

const TouchableHighlightExample = () => {
    const [count, setCount] = useState(0);
  return (
    <View style={{padding:100}}>
      <Text>You clicked {count} times</Text>
      <TouchableHighlight
      style={styles.button}
      onPress={() => setCount(count+1)}>
          <Text>Count</Text>
      </TouchableHighlight>
    </View>
  )
}

export default TouchableHighlightExample

const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10
    }
})