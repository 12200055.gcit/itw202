import React, {useState} from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native'

const TouchableWithoutFeedbackExample = () => {
    const [count, setCount] = useState(0);
  return (
    <View style={{padding:100}}>
        <Text>You clicked {count} times</Text>
        <TouchableWithoutFeedback
        style={styles.button}
        onPress={() => setCount(count + 1)}>
            <Text>Count</Text>
        </TouchableWithoutFeedback>
    </View>
  )
}

export default TouchableWithoutFeedbackExample

const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10
    }
})