import { View, Text, Button } from 'react-native'
import React, {useState} from 'react'

const ButtonExampple = () => {
    const [count, setCount] = useState(0);
  return (
    <View>
      <Text>You clicked {count} times</Text>
      <Button
      onPress={() => setCount(count + 1)}
      title='Count'
      />
    </View>
  )
}

export default ButtonExampple