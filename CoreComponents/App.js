import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button } from 'react-native';
import ViewComponent from './components/ViewComponent';
import ButtonExampple from './components/ButtonExampple';
import ImageComponent from './components/ImageComponent';
import TextComponent from './components/TextComponent';
import TextInputComponent from './components/TextInputComponent';
import TouchableHighlightExample from './components/TouchableHighlightExample';
import TouchableNativeFeedbackExample from './components/TouchableNativeFeedbackExample';
import TouchableOpacityExample from './components/TouchableOpacityExample';
import TouchableWithoutFeedbackExample from './components/TouchableWithoutFeedbackExample';
import CustomComponent from './components/CustomComponents';

export default function App() {
  return (
    // <ScrollView>
    //   <Text>Some text</Text>
    //   <View>
    //     <Text>Some more text</Text>
    //     <Image
    //       source={{
    //         uri: 'https://picsum.photos/64/64',
    //       }} />
    //   </View>
    //   <TextInput 
    //     defaultValue='You can type here' />
    //   <Button
    //     onPress={() => {
    //       alert('You tapped the button!');
    //     }}
    //     title="Press Me" />
    // </ScrollView>

    <View>

      {/* <ViewComponent /> */}
      {/* <ButtonExampple /> */}
      {/* <ImageComponent /> */}
      {/* <TextComponent /> */}
      {/* <TextInputComponent /> */}
      {/* <TouchableHighlightExample /> */}
      {/* <TouchableNativeFeedbackExample /> */}
      {/* <TouchableOpacityExample /> */}
      {/* <TouchableWithoutFeedbackExample /> */}
      <CustomComponent />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
