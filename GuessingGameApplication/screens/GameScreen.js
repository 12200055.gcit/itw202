import { StyleSheet, Text, View, Alert, FlatList, useWindowDimensions } from 'react-native'
import React, {useState, useEffect} from 'react'
import Title from '../component/ui/Title'
import NumberContainer from '../component/game/NumberContainer';
import PrimaryButton from '../component/ui/PrimaryButton';
import Card from '../component/ui/Card';
import InstructionText from '../component/ui/InstructionText';
import {Ionicons} from  '@expo/vector-icons'
import GuessLogItem from '../component/game/GuessLogItem';

function generateRandomBetween(min, max, exclude){
  const rndNum = Math.floor(Math.random() * (max-min)) + min;

  if (rndNum == exclude){
    return generateRandomBetween(min, max, exclude)
  }
  else{
    return rndNum;
  }
}
let minBoundary = 1;
let maxBoundary = 100;

function GameScreen({userNumber, onGameOver}){
  const initialGuess = generateRandomBetween(1, 100, userNumber)
  const [currentGuess, setCurrentGuess] = useState(initialGuess)
  const [guessRounds, setGuessRounds] = useState([initialGuess]);
  const {width, height} = useWindowDimensions();

useEffect(() => {
  if (currentGuess === userNumber){
    onGameOver(guessRounds.length);
  }
},[currentGuess, userNumber, onGameOver])

useEffect(() => {
  minBoundary = 1;
  maxBoundary = 100;
}, [])

  function nextGuessHandler(direction){
    if(
      (direction === 'lower' && currentGuess < userNumber) ||
      (direction === 'greater' && currentGuess > userNumber))
      {
        Alert.alert("Don't lie!", 'You know that this wrong...',[
          {text: 'Sorry', style: 'cancel'}
        ])
        return;
    }

    if (direction === 'lower'){
      maxBoundary = currentGuess;
    }
    else{
      minBoundary = currentGuess + 1;
    }
    console.log(minBoundary, maxBoundary)
    const newRndNumber = generateRandomBetween(minBoundary, maxBoundary, currentGuess)

    setCurrentGuess(newRndNumber);
    setGuessRounds((prevGuessRounds => [newRndNumber, ...prevGuessRounds]))
  }

  const guessRoundsListLength = guessRounds.length

  let content = 
    <>  
      <NumberContainer>{currentGuess}</NumberContainer>
      <Card>
          <InstructionText style={styles.instructionText}>Higher or lower?</InstructionText> 
          <View style={styles.buttonsContainer}>
            <View style={styles.buttonConatiner}>
            <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
              <Ionicons name='md-add' size={24} color='white' />
            </PrimaryButton>
            </View>
            <View style={styles.buttonConatiner}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                <Ionicons name='md-remove' size={24} color='white' />
              </PrimaryButton>
            </View>
          </View>
      </Card>
    </>   //this is called a 'fragment' which works as a container and it can be used when want to wrap the two components which lets you use the two root components without having to wrap it in a <view/>

  

  if (width > 500){
    content = (
      <>
        <View style={styles.buttonsContainerWide}>
          <View style={styles.buttonConatiner}>
            <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
              <Ionicons name='md-add' size={24} color='white' />
            </PrimaryButton>
          </View>

          <NumberContainer>{currentGuess}</NumberContainer>
          <View style={styles.buttonConatiner}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                <Ionicons name='md-remove' size={24} color='white' />
              </PrimaryButton>
          </View>
        </View>
      </>
    )
  }

  return (
    <View style={styles.screen}>
      <Title>Opponent's Guess</Title>
      {content}
      <View style={styles.listContainer}>
        <FlatList
        data={guessRounds}
        renderItem={(itemData) => 
        <GuessLogItem 
        roundNumber={guessRoundsListLength - itemData.index}
        guess = {itemData.item}
        />
        }
        keyExtractor={(item) => item}
        />
      </View>
    </View>
  )
}

export default GameScreen;


const styles = StyleSheet.create({
  instructionText: {
    marginBottom: 12
  },
  screen: {
    flex: 1,
    padding: 24, 
    alignItems: 'center'
  },
  buttonsContainer: {
    flexDirection: 'row'
  },
  buttonConatiner: {
    flex: 1,
  },
  buttonsContainerWide: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  listContainer: {
    flex: 1,
    padding: 12
  }
})