import {useState} from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import StartGameScreen from './screens/StartGameScreen';
import { LinearGradient } from 'expo-linear-gradient';
import GameScreen from './screens/GameScreen';
import Colors from './constants/Colors';
import GameOverScreen from './screens/GameOverScreen';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';
import { StatusBar } from 'expo-status-bar';


export default function App() {
  const [userNumber, setUserNumber] = useState()
  const [gameIsOver, setGameIsOver] = useState(true)
  const [guessRounds, setGuessRounds] = useState(0)

  const [fontsLoaded] = useFonts({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  })

  if(!fontsLoaded){
    return <AppLoading />
  }
  function pickedNumberHandler(pickerNumber){
    setUserNumber(pickerNumber);
    setGameIsOver(false);
  }

  function gameOverHandler(numberOfRounds){
    setGameIsOver(true);
    setGuessRounds(numberOfRounds);
  }

  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRounds(0);
    
  }

  let screen = <StartGameScreen onPickNumber = {pickedNumberHandler} />
  if (userNumber){
    screen = <GameScreen userNumber={userNumber} onGameOver = {gameOverHandler}/>
  }
  if (gameIsOver && userNumber){
    screen = <GameOverScreen 
                userNumber={userNumber}
                roundsNumber={guessRounds}
                onStartNewGame={startNewGameHandler}
                
                />
  }


  return (
    <>
      <StatusBar style='dark'/>
      <LinearGradient
        style={styles.container}
        colors={[Colors.primary700, Colors.accent500]}
      >
      <SafeAreaView style={styles.container}>
        {screen}
      </SafeAreaView>
        
      </LinearGradient>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
});
