import React from "react";
import { View, Text, StyleSheet } from "react-native";

const TextComp = () => {
    return (
        <View>
            <Text style={styles.baseText}>
                The 
                <Text style={styles.innerText}> quick brown fox </Text>
                <Text>jumps over the lazy dog </Text>
            </Text>
        </View>
    );
};

export default TextComp

const styles = StyleSheet.create({
    baseText: {
        fontSize: 16,
    },
    innerText: {
        fontWeight: "bold",
    }
})