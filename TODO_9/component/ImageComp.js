import React from 'react'
import { View, Image, StyleSheet } from 'react-native';

const ImageComp = () => {
    return (
        <View>
            <Image
             style={styles.logoContain}
             source={require('../assets/react_native_logo.png')} />
             <Image
              style={styles.logoStretch}
              source={{uri: 'https://picsum.photos/100/100'}} />
        </View>
    )
}

export default ImageComp

const styles = StyleSheet.create({
    logoContain: {
        width: 100, 
        height: 100,
        resizeMode: 'contain',
    },
    logoStretch: {
        width: 100, 
        height: 100,
        resizeMode: 'contain',
    },
});