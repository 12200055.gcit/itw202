import { StyleSheet, Text, View, Image } from 'react-native';
import ImageComp from './component/ImageComp';
export default function App() {
  return (
    <View style={styles.container}>
        <ImageComp></ImageComp>
   
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
