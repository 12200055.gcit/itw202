import React, {useState} from "react";
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const MyButton=({buttontext})=>{
    const [text, changeText]=useState(' The button isnt pressed yet!' );
    const [count, setCount] = useState(1);
    const [isDisabebled, setIsDisabled] =useState(false);

    const ChangeState=()=>{
        if (count == 3) {
            setIsDisabled(true)+changeText('The button was pressed '+count+' times')
           
      }
      else{
          setCount(count+1) +changeText('The button was pressed '+count+' times')
      }

    }
    return(
        <View>
            <Text>{text}</Text>
           <TouchableOpacity onPress={()=>{ChangeState()}} disabled={isDisabebled} style={styles.button}>
               <Text style={styles.buttontext}>{buttontext}</Text>
               </TouchableOpacity>

        </View>
    )
}

const styles=StyleSheet.create({
    button:{
        alignItems:'center',
        backgroundColor:'#2196f3',
        padding:10,
    },
    buttontext:{
        color:'white'
    }
});

export default MyButton;
