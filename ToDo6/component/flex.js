import React from "react";
import { View, Text, StyleSheet } from 'react-native';

const Flex = () => {
    return(
        <View style = {styles.container}>
            <View style = {styles.box1}></View>
            <View style = {styles.box2}></View>
            <View style = {styles.box3}></View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginBottom: 400,
        marginRight: 35,
    },
    box1: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff3333',
        width: 50,
        height: 200,
    },
    box2: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'royalblue',
        width: 150,
        height: 200,
    },
    box3: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'green',
        width: 10,
        height: 200,
    },
});
export default Flex;